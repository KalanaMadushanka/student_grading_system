package lk.interview.grading_managment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradingManagment {

    public static void main(String[] args) {
        SpringApplication.run(GradingManagment.class, args);
    }

}
