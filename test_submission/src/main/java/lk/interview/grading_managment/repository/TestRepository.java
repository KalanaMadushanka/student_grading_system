package lk.interview.grading_managment.repository;

import org.springframework.data.repository.CrudRepository;
import lk.interview.grading_managment.model.Test;

public interface TestRepository extends CrudRepository<Test, Long> {
    Iterable<Test> findAllByTeacherId(Long id);
}
