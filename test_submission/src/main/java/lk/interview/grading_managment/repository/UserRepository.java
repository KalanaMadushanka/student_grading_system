package lk.interview.grading_managment.repository;

import lk.interview.grading_managment.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends UserBaseRepository<User> {
}
