package lk.interview.grading_managment.repository;

import lk.interview.grading_managment.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role,Long> {
    Iterable<Role> findAll();
    Role getRoleByName(String roleName);
}
