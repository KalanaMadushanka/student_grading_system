package lk.interview.grading_managment.repository;

import lk.interview.grading_managment.model.Answer;
import org.springframework.data.repository.CrudRepository;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
    Answer findById(long id);
}
