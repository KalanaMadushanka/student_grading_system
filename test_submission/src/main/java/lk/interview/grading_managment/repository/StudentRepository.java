package lk.interview.grading_managment.repository;

import org.springframework.stereotype.Repository;
import lk.interview.grading_managment.model.Student;

@Repository
public interface StudentRepository extends UserBaseRepository<Student> {
}
