package lk.interview.grading_managment.repository;

import lk.interview.grading_managment.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface UserBaseRepository<T extends User> extends CrudRepository<T, Long> {
    T findUserByUsername(String s);

    T findUserByUsernameAndIdNotLike(String s, long id);

    T findUserByEmail(String s);

    T findUserByEmailAndIdNotLike(String s, long id);

    T findUserById(long id);

    Iterable<T> findAll();

    Iterable<T> findAllByRole_NameEquals(String roleName);
}
