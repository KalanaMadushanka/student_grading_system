package lk.interview.grading_managment.repository;

import org.springframework.data.repository.CrudRepository;
import lk.interview.grading_managment.model.Question;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    Iterable<Question> findAllByTeacherId(long id);
}
